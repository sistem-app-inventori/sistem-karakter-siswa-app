import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RekapkepsekPageRoutingModule } from './rekapkepsek-routing.module';

import { RekapkepsekPage } from './rekapkepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RekapkepsekPageRoutingModule
  ],
  declarations: [RekapkepsekPage]
})
export class RekapkepsekPageModule {}
