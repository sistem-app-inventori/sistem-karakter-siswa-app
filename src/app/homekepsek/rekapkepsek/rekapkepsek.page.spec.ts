import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RekapkepsekPage } from './rekapkepsek.page';

describe('RekapkepsekPage', () => {
  let component: RekapkepsekPage;
  let fixture: ComponentFixture<RekapkepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapkepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RekapkepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
