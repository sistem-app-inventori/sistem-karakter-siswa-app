import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RekapkepsekPage } from './rekapkepsek.page';

const routes: Routes = [
  {
    path: '',
    component: RekapkepsekPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RekapkepsekPageRoutingModule {}
