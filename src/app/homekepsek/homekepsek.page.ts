import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { RekapkepsekPage } from './rekapkepsek/rekapkepsek.page';

@Component({
  selector: 'app-homekepsek',
  templateUrl: './homekepsek.page.html',
  styleUrls: ['./homekepsek.page.scss'],
})
export class HomekepsekPage implements OnInit {

  constructor(
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async rekapdata() {
    const modal = await this.modalController.create({
      component: RekapkepsekPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
