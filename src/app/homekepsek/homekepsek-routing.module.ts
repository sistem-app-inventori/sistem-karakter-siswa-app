import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomekepsekPage } from './homekepsek.page';

const routes: Routes = [
  {
    path: '',
    component: HomekepsekPage
  },  {
    path: 'rekapkepsek',
    loadChildren: () => import('./rekapkepsek/rekapkepsek.module').then( m => m.RekapkepsekPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomekepsekPageRoutingModule {}
