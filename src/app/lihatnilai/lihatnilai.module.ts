import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LihatnilaiPageRoutingModule } from './lihatnilai-routing.module';

import { LihatnilaiPage } from './lihatnilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LihatnilaiPageRoutingModule
  ],
  declarations: [LihatnilaiPage]
})
export class LihatnilaiPageModule {}
