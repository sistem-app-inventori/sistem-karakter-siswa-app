import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeortuPageRoutingModule } from './homeortu-routing.module';

import { HomeortuPage } from './homeortu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeortuPageRoutingModule
  ],
  declarations: [HomeortuPage]
})
export class HomeortuPageModule {}
