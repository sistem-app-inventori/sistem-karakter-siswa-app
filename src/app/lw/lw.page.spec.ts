import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LwPage } from './lw.page';

describe('LwPage', () => {
  let component: LwPage;
  let fixture: ComponentFixture<LwPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LwPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LwPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
