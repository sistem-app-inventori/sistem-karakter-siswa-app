import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LsPageRoutingModule } from './ls-routing.module';

import { LsPage } from './ls.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LsPageRoutingModule
  ],
  declarations: [LsPage]
})
export class LsPageModule {}
