import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LsPage } from './ls.page';

const routes: Routes = [
  {
    path: '',
    component: LsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LsPageRoutingModule {}
