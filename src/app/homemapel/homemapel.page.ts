import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ProfilPage } from '../profil/profil.page';
import { InputnilaiPage } from '../inputnilai/inputnilai.page';
import { SoalPage } from './soal/soal.page';
import { JadwalPenilaianPage } from './jadwal-penilaian/jadwal-penilaian.page';

@Component({
  selector: 'app-homemapel',
  templateUrl: './homemapel.page.html',
  styleUrls: ['./homemapel.page.scss'],
})
export class HomemapelPage implements OnInit {

  constructor(
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async profil() {
    const modal = await this.modalController.create({
      component: ProfilPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async jadwalmapel() {
    const modal = await this.modalController.create({
      component: JadwalPenilaianPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async soal() {
    const modal = await this.modalController.create({
      component: SoalPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async input() {
    const modal = await this.modalController.create({
      component: InputnilaiPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
