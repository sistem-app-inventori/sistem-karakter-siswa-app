import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { HomemapelPage } from '../homemapel.page';

@Component({
  selector: 'app-soal',
  templateUrl: './soal.page.html',
  styleUrls: ['./soal.page.scss'],
})
export class SoalPage implements OnInit {

  data: any = {};
  userData: any = {};
  soal: any = [];
  
  constructor(
    public router: Router,
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    public modalController: ModalController
    
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(user=>{
      this.userData = user;
      this.getsoal();
    })
  }
  selectedSegments: any='soal';

  async home() {
    const modal = await this.modalController.create({
      component: HomemapelPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

 loading: boolean;
 addsoal(){
  this.loading = true;
    this.data.author = this.userData.email;
    var id = new Date().getTime().toString();
    this.db.collection('data_soal').doc(id).set(this.data).then(res=>{
      this.loading= false;
      alert('Soal berhasil ditambahkan');
    }).catch(err=>{
      this.loading = false;
      alert('Tidak dapat menambahkan soal');
    })
  }

  getsoal(){
    this.soal = [];
    this.db.collection('data_soal').valueChanges({idField: 'id'}).subscribe(res => {
      this.soal = res;
    })
  }

    hapus(id: any) {
      var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
      if (r == true) {
        this.db.collection('data_soal').doc(id).delete();
        alert('Pengguna baru berhasil dihapus');
      } else {
        return;
      }
    }


}
