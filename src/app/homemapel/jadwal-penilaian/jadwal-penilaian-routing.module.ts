import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JadwalPenilaianPage } from './jadwal-penilaian.page';

const routes: Routes = [
  {
    path: '',
    component: JadwalPenilaianPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JadwalPenilaianPageRoutingModule {}
