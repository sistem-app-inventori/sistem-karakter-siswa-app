import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JadwalPenilaianPage } from './jadwal-penilaian.page';

describe('JadwalPenilaianPage', () => {
  let component: JadwalPenilaianPage;
  let fixture: ComponentFixture<JadwalPenilaianPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JadwalPenilaianPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JadwalPenilaianPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
