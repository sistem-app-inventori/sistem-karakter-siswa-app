import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HalPenangananPage } from './hal-penanganan.page';

describe('HalPenangananPage', () => {
  let component: HalPenangananPage;
  let fixture: ComponentFixture<HalPenangananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalPenangananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HalPenangananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
