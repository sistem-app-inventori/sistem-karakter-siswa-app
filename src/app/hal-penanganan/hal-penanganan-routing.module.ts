import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HalPenangananPage } from './hal-penanganan.page';

const routes: Routes = [
  {
    path: '',
    component: HalPenangananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HalPenangananPageRoutingModule {}
