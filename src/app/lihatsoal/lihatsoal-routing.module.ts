import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LihatsoalPage } from './lihatsoal.page';

const routes: Routes = [
  {
    path: '',
    component: LihatsoalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LihatsoalPageRoutingModule {}
