import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LihatsoalPage } from './lihatsoal.page';

describe('LihatsoalPage', () => {
  let component: LihatsoalPage;
  let fixture: ComponentFixture<LihatsoalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LihatsoalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LihatsoalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
