import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lk',
  templateUrl: './lk.page.html',
  styleUrls: ['./lk.page.scss'],
})
export class LkPage implements OnInit {

  user: any = {};
  constructor(
    private auth: AngularFireAuth,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  loading: boolean;
  login(){
    this.loading = true;
    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.router.navigate(['/homekepsek']);
    }).catch(err=>{
      this.loading = false;
      alert('Tidak dapat login');
    });
  }
}
