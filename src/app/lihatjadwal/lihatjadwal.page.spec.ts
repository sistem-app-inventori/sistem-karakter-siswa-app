import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LihatjadwalPage } from './lihatjadwal.page';

describe('LihatjadwalPage', () => {
  let component: LihatjadwalPage;
  let fixture: ComponentFixture<LihatjadwalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LihatjadwalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LihatjadwalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
