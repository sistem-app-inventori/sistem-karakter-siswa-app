import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LihatjadwalPage } from './lihatjadwal.page';

const routes: Routes = [
  {
    path: '',
    component: LihatjadwalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LihatjadwalPageRoutingModule {}
