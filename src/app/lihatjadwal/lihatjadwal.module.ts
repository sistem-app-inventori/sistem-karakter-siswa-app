import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LihatjadwalPageRoutingModule } from './lihatjadwal-routing.module';

import { LihatjadwalPage } from './lihatjadwal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LihatjadwalPageRoutingModule
  ],
  declarations: [LihatjadwalPage]
})
export class LihatjadwalPageModule {}
