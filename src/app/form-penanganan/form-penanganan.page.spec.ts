import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormPenangananPage } from './form-penanganan.page';

describe('FormPenangananPage', () => {
  let component: FormPenangananPage;
  let fixture: ComponentFixture<FormPenangananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPenangananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormPenangananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
