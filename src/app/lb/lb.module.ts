import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LbPageRoutingModule } from './lb-routing.module';

import { LbPage } from './lb.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LbPageRoutingModule
  ],
  declarations: [LbPage]
})
export class LbPageModule {}
