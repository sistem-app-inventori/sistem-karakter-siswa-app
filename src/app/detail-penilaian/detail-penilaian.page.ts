import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { RekapdataPage } from '../rekapdata/rekapdata.page';

@Component({
  selector: 'app-detail-penilaian',
  templateUrl: './detail-penilaian.page.html',
  styleUrls: ['./detail-penilaian.page.scss'],
})
export class DetailPenilaianPage implements OnInit {
  
  students: any[];
  constructor(
    public db: AngularFirestore,
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    fetch('./assets/student.json').then(res => res.json())
      .then(json => {
        this.students = json;
      });
  }

  async rekapdata() {
    const modal = await this.modalController.create({
      component: RekapdataPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
