import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailPenilaianPage } from './detail-penilaian.page';

describe('DetailPenilaianPage', () => {
  let component: DetailPenilaianPage;
  let fixture: ComponentFixture<DetailPenilaianPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPenilaianPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailPenilaianPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
