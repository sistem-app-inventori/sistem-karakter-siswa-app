import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RekapdataPage } from './rekapdata.page';

const routes: Routes = [
  {
    path: '',
    component: RekapdataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RekapdataPageRoutingModule {}
