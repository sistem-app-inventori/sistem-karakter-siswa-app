import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RekapdataPageRoutingModule } from './rekapdata-routing.module';

import { RekapdataPage } from './rekapdata.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RekapdataPageRoutingModule
  ],
  declarations: [RekapdataPage]
})
export class RekapdataPageModule {}
