import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditNilaiPageRoutingModule } from './edit-nilai-routing.module';

import { EditNilaiPage } from './edit-nilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditNilaiPageRoutingModule
  ],
  declarations: [EditNilaiPage]
})
export class EditNilaiPageModule {}
